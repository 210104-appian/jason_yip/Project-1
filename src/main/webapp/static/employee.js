window.onload = function() {
    console.log("employee.js file is working!")
}

document.getElementById("createReimbursementBtn").addEventListener("click", createReimbursement);

function createReimbursement() {
    event.preventDefault();
    console.log("got into createReimbursement method");

    let userId = sessionStorage.getItem("userId");
    let firstName = sessionStorage.getItem("firstName");
    let lastName = sessionStorage.getItem("lastName");
    let amount = document.getElementById("amountInput").value;
    let type = document.getElementById("typeInput").value;
    let status = 'PENDING';

    let request = {userId, firstName, lastName, amount, type, status};
    const requestJSON = JSON.stringify(request);

    console.log(requestJSON);
    console.log(createRrUrl);
    performAjaxPostRequest(createRrUrl, requestJSON, handleSuccessfulRequest, handleUnsuccessfulRequest);
    console.log("hit the end of the create reimbursement function");
    document.getElementById("successText").hidden=false;

}

function handleSuccessfulRequest() {
    document.getElementById("successText").hidden=false;
    console.log("Success!");
}

function handleUnsuccessfulRequest() {
    console.log("Failure");
    document.getElementById("failureText").hidden=false;
}