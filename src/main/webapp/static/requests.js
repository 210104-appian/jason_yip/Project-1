window.onload = function() {
    console.log("javascript is working");
    console.log(sessionStorage.getItem("userId"));
}

const users = [
    {id: 0, username: "username", password: "password", firstname: "firstname", lastname: "lastname"},
    {id: 1, username: "username", password: "password", firstname: "firstname", lastname: "lastname"}
];

//let testUsers;

//document.getElementById("something-btn").addEventListener("click", buttonClicked);
document.getElementById("homepageBtn").addEventListener("click", determineHomepageLanding);
document.getElementById("reimbursements-btn").addEventListener("click", getAllReimbursements(renderReimbursementRequests));
document.getElementById("table-btn").addEventListener("click", testRenderUsers);
document.getElementById("table-btn2").addEventListener("click", getAllUsers(renderDbUsers));

function renderReimbursementRequests(jsonReimbursements) {
    //event.preventDefault();
    console.log("started renderReimbursementRequests()");

    reimbursements = JSON.parse(jsonReimbursements);

    let table = document.getElementById("reimbursement-table");
    console.log(table);
    for (let reimbursement of reimbursements) {
        console.log("got in loop");
        console.log(table);
        let newRow = document.createElement("tr");
        let newReimbursementId = document.createElement("td");
        let newEmpId = document.createElement("td");
        let newFirstName = document.createElement("td");
        let newLastName = document.createElement("td");
        let newAmount = document.createElement("td");
        let newType = document.createElement("td");
        let newStatus = document.createElement("td");

        newReimbursementId.innerHTML = reimbursement.requestId;
        newEmpId.innerHTML = reimbursement.userId;
        newFirstName.innerHTML = reimbursement.firstName;
        newLastName.innerHTML = reimbursement.lastName;
        newAmount.innerHTML = reimbursement.amount;
        newType.innerHTML = reimbursement.type;
        newStatus.innerHTML = reimbursement.status;

        newRow.appendChild(newReimbursementId);
        newRow.appendChild(newEmpId);
        newRow.appendChild(newFirstName);
        newRow.appendChild(newLastName);
        newRow.appendChild(newAmount);
        newRow.appendChild(newType);
        newRow.appendChild(newStatus);
        table.appendChild(newRow);
    }
    console.log(table);
    console.log("renderReimbursementRequests ended");
}

function testRenderUsers() {
    console.log("testRenderUsers started")
    let testTable = document.getElementById("test-table");
    for (let testUser of users) {
        console.log("got in loop");
        let newRow = document.createElement("tr");
        let newEmpId = document.createElement("td");
        let newUsername = document.createElement("td");
        let newPassword = document.createElement("td");
        let newFirstName = document.createElement("td");
        let newLastName = document.createElement("td");

        newEmpId.innerHTML = testUser.id;
        newUsername.innerHTML = testUser.username;
        newPassword.innerHTML = testUser.password;
        newFirstName.innerHTML = testUser.firstname;
        newLastName.innerHTML = testUser.lastname;

        newRow.appendChild(newEmpId);
        newRow.appendChild(newUsername);
        newRow.appendChild(newPassword);
        newRow.appendChild(newFirstName);
        newRow.appendChild(newLastName);
        testTable.appendChild(newRow);
    }
    console.log(testTable);
    console.log("testRenderUsers ended");
}

function renderDbUsers(jsonUsers) {

    dbUsers = JSON.parse(jsonUsers);

    console.log("renderUsers started")
    let testTable = document.getElementById("test-table");
    for (let testUser of dbUsers) {
        console.log("got in loop");
        let newRow = document.createElement("tr");
        let newEmpId = document.createElement("td");
        let newUsername = document.createElement("td");
        let newPassword = document.createElement("td");
        let newFirstName = document.createElement("td");
        let newLastName = document.createElement("td");

        newEmpId.innerHTML = testUser.id;
        newUsername.innerHTML = testUser.username;
        newPassword.innerHTML = testUser.password;
        newFirstName.innerHTML = testUser.firstname;
        newLastName.innerHTML = testUser.lastname;

        newRow.appendChild(newEmpId);
        newRow.appendChild(newUsername);
        newRow.appendChild(newPassword);
        newRow.appendChild(newFirstName);
        newRow.appendChild(newLastName);
        testTable.appendChild(newRow);
    }
    console.log(testTable);
    console.log("testRenderUsers ended");
}

function buttonClicked() {
    let greetingHeader = document.getElementsByClassName("greeting-header");
    document.getElementById("greeting-header").innerText = "Welcome, button clicker!";
    console.log(greetingHeader);
}