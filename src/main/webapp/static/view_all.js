window.onload = function() {
    console.log("in window.onload");
    getAllReimbursements(renderReimbursementRequests);
}

function renderReimbursementRequests(jsonReimbursements) {
    //event.preventDefault();
    console.log("started renderReimbursementRequests()");

    reimbursements = JSON.parse(jsonReimbursements);

    let table = document.getElementById("reimbursement-table");
    console.log(table);
    for (let reimbursement of reimbursements) {
        console.log("got in loop");
        console.log(table);
        let newRow = document.createElement("tr");
        let newReimbursementId = document.createElement("td");
        let newEmpId = document.createElement("td");
        let newFirstName = document.createElement("td");
        let newLastName = document.createElement("td");
        let newAmount = document.createElement("td");
        let newType = document.createElement("td");
        let newStatus = document.createElement("td");

        newReimbursementId.innerHTML = reimbursement.requestId;
        newEmpId.innerHTML = reimbursement.userId;
        newFirstName.innerHTML = reimbursement.firstName;
        newLastName.innerHTML = reimbursement.lastName;
        newAmount.innerHTML = reimbursement.amount;
        newType.innerHTML = reimbursement.type;
        newStatus.innerHTML = reimbursement.status;

        newRow.appendChild(newReimbursementId);
        newRow.appendChild(newEmpId);
        newRow.appendChild(newFirstName);
        newRow.appendChild(newLastName);
        newRow.appendChild(newAmount);
        newRow.appendChild(newType);
        newRow.appendChild(newStatus);
        table.appendChild(newRow);
    }
    console.log(table);
    console.log("renderReimbursementRequests ended");
}