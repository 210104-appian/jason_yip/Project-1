window.onload = function() {
    console.log("javascript is working!");
}

document.getElementById("homepageBtn").addEventListener("click", determineHomepageLanding);
document.getElementById("logoutBtn").addEventListener("click", logOut);
function determineHomepageLanding() {
    console.log("In determineHomepageLanding");
    let tempRole = sessionStorage.getItem("role")
    if (tempRole == "EMPLOYEE") {
        window.location.href="employee_homepage.html";
        console.log("Redirecting to the employee homepage")
    } else if (tempRole == "MANAGER") {
        window.location.href="manager_homepage.html";
        console.log("Redirecting to the manager homepage")
    } else {
        window.location.href="login.html";
    }
}

function logOut() {
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("userId");
    sessionStorage.removeItem("firstName");
    sessionStorage.removeItem("lastName");
    sessionStorage.removeItem("role");
    window.location.href="login.html";
    console.log("Logged Out!")
}