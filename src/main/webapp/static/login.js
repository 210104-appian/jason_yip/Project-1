document.getElementById("loginBtn").addEventListener("click", attemptLogin);

function attemptLogin() {
    let username = document.getElementById("usernameInput").value;
    let password = document.getElementById("passwordInput").value;
    let credentials = {username, password};
    performAjaxPostRequest(loginUrl, JSON.stringify(credentials), handleSuccessfulLogin, handleUnsuccessfulLogin);
    console.log("hit end of attemptLogin()");
}

function handleSuccessfulLogin(responseText) {
    console.log("You've logged in successfully");
    let token = responseText;
    tokenJSON = JSON.parse(token);
    sessionStorage.setItem("token", tokenJSON);
    sessionStorage.setItem("userId", tokenJSON.id)
    sessionStorage.setItem("firstName", tokenJSON.firstName);
    sessionStorage.setItem("lastName", tokenJSON.lastName);
    sessionStorage.setItem("role", tokenJSON.role);

    document.getElementById("loginFailureMsg").hidden=true;
    document.getElementById("loginSuccessMsg").hidden=false;
    document.getElementById("logoutBtn").hidden=false;
    document.getElementsByClassName("logoutBtn").hidden = false;
    determineHomepageLanding();
}

function handleUnsuccessfulLogin(){
    document.getElementById("loginSuccessMsg").hidden=true;
    document.getElementById("loginFailureMsg").hidden=false;
    console.log("Login unsuccessful");
}
