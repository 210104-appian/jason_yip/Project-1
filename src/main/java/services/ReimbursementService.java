package services;
import models.ReimbursementRequest;
import java.util.List;
import daos.ReimbursementDao;
import daos.ReimbursementDaoImpl;

public class ReimbursementService {

	private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
	
	public List<ReimbursementRequest> getAllReimbursements() {
		return reimbursementDao.getAllReimbursements();
	}
	
	public List<ReimbursementRequest> viewApprovedReimbursements(int eId) {
		return reimbursementDao.viewApprovedReimbursements(eId);
	}
	
	public void createReimbursementRequest(int eId, String fName, String lName, double amount, String type, String status) {
		reimbursementDao.createReimbursementRequest(eId, fName, lName, amount, type, status);
	}
	
}
