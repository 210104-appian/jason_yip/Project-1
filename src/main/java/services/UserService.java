package services;
import java.util.ArrayList;
import java.util.List;

import daos.ReimbursementDao;
import daos.ReimbursementDaoImpl;
import daos.UserDao;
import daos.UserDaoImpl;
import models.User;

public class UserService {

	public UserDao userDao = new UserDaoImpl();
	public ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
	
	public User login(String username, String password) {
		return userDao.login(username, password);
	}
	
	public void createReimbursementRequest(int eId, String fName, String lName, double amount, String type, String status) {
		reimbursementDao.createReimbursementRequest(eId, fName, lName, amount, type, status);
	}
	
	public User findUserByUsernameAndPassword(String username, String password) {
		System.out.println("We're looking for a matching username and password");
		List<User> users = new ArrayList<>();
		users = userDao.getUsers();
		for(User user: users) {
			System.out.println("Checking if this user is a match: " + user);
			if(user!=null) {
				if(user.getUsername()!=null && user.getUsername().equals(username)) {
					if(user.getPassword()!=null && user.getPassword().equals(password)) {
						return user;
					}
				}
			}
		}
		return null;
	}
}
