package services;
import java.util.List;
import models.ReimbursementRequest;
import daos.ReimbursementDao;
import daos.ReimbursementDaoImpl;

public class EmployeeService {
	
	public ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
	
	public void createReimbursementRequest(int eId, String fName, String lName, double amount, String type, String status) {
		reimbursementDao.createReimbursementRequest(eId, fName, lName, amount, type, status);
	}
}
