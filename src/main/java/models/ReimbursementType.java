package models;

public enum ReimbursementType {
	TRAVEL, FOOD, HOUSING, CERTIFICATION, OTHER;
}
