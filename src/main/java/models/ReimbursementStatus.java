package models;

public enum ReimbursementStatus {

	APPROVED, DENIED, PENDING;
}
