package daos;

import java.util.List;

import models.ReimbursementRequest;

public interface ReimbursementDao {

	public List<ReimbursementRequest> getAllReimbursements();
	public List<ReimbursementRequest> viewPendingReimbursements(int eId);
	public List<ReimbursementRequest> viewApprovedReimbursements(int eId);
	public List<ReimbursementRequest> viewDeniedReimbursements(int eId);
	public void createReimbursementRequest(int eId, String fName, String lName, double amount, String type, String status);
}
