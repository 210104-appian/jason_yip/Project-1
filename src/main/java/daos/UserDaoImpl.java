package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.User;
import models.UserRole;
import util.ConnectionUtil;

public class UserDaoImpl implements UserDao{

	@Override
	public List<User> getUsers() {
		List<User> users = new ArrayList<>();
		String sql = "SELECT * FROM USERS";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			ResultSet rs = pStatement.executeQuery();
			users = processUserResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
	
	@Override
	public User login(String username, String password) {
		String sql = ("SELECT USER_ID, FIRST_NAME, LAST_NAME, USER_ROLE FROM USERS WHERE USERNAME = ? AND PASSWORD = ?");
		User user = new User();
		try (Connection connection = ConnectionUtil.getConnection()) {
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setString(1,  username);
			pStatement.setString(2, password);
			ResultSet rs = pStatement.executeQuery();
			return tryLogin(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	private User tryLogin(ResultSet resultSet) throws SQLException {
		User user = new User();
		while (resultSet.next()) {
			user.setId(resultSet.getInt("USER_ID"));
			user.setFirstName(resultSet.getString("FIRST_NAME"));
			user.setLastName(resultSet.getString("LAST_NAME"));
			
			String role = resultSet.getString("USER_ROLE");
			UserRole userRole = UserRole.valueOf(role);
			user.setRole(userRole);
		}
		return user;
	}
	
	private List<User> processUserResultSet(ResultSet resultSet) throws SQLException {
		List<User> users = new ArrayList<>();
		while (resultSet.next()) {
			User user = new User();
			user.setId(resultSet.getInt("USER_ID"));
			user.setFirstName(resultSet.getString("FIRST_NAME"));
			user.setLastName(resultSet.getString("LAST_NAME"));
			user.setUsername(resultSet.getString("USERNAME"));
			user.setPassword(resultSet.getString("PASSWORD"));
			
			String role = resultSet.getString("USER_ROLE");
			UserRole uRole = UserRole.valueOf(role);
			user.setRole(uRole);
			users.add(user);
		}
		return users;
	}
}
