package daos;

import java.util.List;

import models.User;

public interface UserDao {
	
	public User login(String username, String password);
	public List<User> getUsers();
}
