package daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import util.ConnectionUtil;
import models.ReimbursementRequest;
import models.ReimbursementStatus;
import models.ReimbursementType;
import models.User;

public class ReimbursementDaoImpl implements ReimbursementDao {

	@Override
	public List<ReimbursementRequest> getAllReimbursements() {
		String sql = ("SELECT * FROM REIMBURSEMENTS");
		List<ReimbursementRequest> requests = new ArrayList<ReimbursementRequest>();
		try (Connection connection = ConnectionUtil.getConnection()) {
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet rs = pStatement.executeQuery();
				return processReimbursementResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
	}

	@Override
	public List<ReimbursementRequest> viewPendingReimbursements(int eId) {
		String sql = ("SELECT * FROM REIMBURSEMENTS WHERE EMP_ID = ? AND STATUS = 'PENDING'");
		List<ReimbursementRequest> requests = new ArrayList<ReimbursementRequest>();
		try (Connection connection = ConnectionUtil.getConnection()) {
			PreparedStatement pStatement = connection.prepareStatement(sql);
			ResultSet rs = pStatement.executeQuery();
			return processReimbursementResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
	}

	@Override
	public List<ReimbursementRequest> viewApprovedReimbursements(int eId) {
		String sql = ("SELECT * FROM REIMBURSEMENTS WHERE EMP_ID = ? AND STATUS = 'APPROVED'");
		List<ReimbursementRequest> requests = new ArrayList<ReimbursementRequest>();
		try (Connection connection = ConnectionUtil.getConnection()) {
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setInt(eId, eId);
			ResultSet rs = pStatement.executeQuery();
			return processReimbursementResultSet(rs);
		} catch (SQLException e) {
			
		}
		return requests;
	}
	
	@Override
	public List<ReimbursementRequest> viewDeniedReimbursements(int eId) {
		return null;
	}
	
	@Override
	public void createReimbursementRequest(int eId, String fName, String lName, double amount, String type, String status) {
		String sql = ("INSERT INTO REIMBURSEMENTS (EMP_ID, FIRST_NAME, LAST_NAME,AMOUNT,R_TYPE,R_STATUS)VALUES (?, ?, ?, ?, ?, ?)");
		try (Connection connection = ConnectionUtil.getConnection()) {
			PreparedStatement pStatement = connection.prepareStatement(sql);
			pStatement.setInt(1, eId);
			pStatement.setString(2, fName);
			pStatement.setString(3, lName);
			pStatement.setDouble(4, amount);
			pStatement.setString(5, type);
			pStatement.setString(6, status);
			pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private List<ReimbursementRequest> processReimbursementResultSet(ResultSet resultSet) throws SQLException {
		List<ReimbursementRequest> reimbursements = new ArrayList<>();
		while (resultSet.next()) {
			ReimbursementRequest Rr = new ReimbursementRequest();
			
			Rr.setRequestId(resultSet.getInt("R_ID"));
			Rr.setUserId(resultSet.getInt("EMP_ID"));
			Rr.setFirstName(resultSet.getString("FIRST_NAME"));
			Rr.setLastName(resultSet.getString("LAST_NAME"));
			Rr.setAmount(resultSet.getDouble("AMOUNT"));
			String type = resultSet.getString("R_TYPE");
			
			ReimbursementType rType = ReimbursementType.valueOf(type);
			Rr.setType(rType);
			String status = resultSet.getString("R_STATUS");
			ReimbursementStatus rStatus = ReimbursementStatus.valueOf(status);
			Rr.setStatus(rStatus);
			reimbursements.add(Rr);
		}
		return reimbursements;
	}
}
