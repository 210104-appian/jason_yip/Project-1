package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import services.ReimbursementService;
import models.ReimbursementRequest;
import models.User;

public class CreateRrServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private ReimbursementService reimbursementService = new ReimbursementService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("GET request to ReimbursementRequest Servlet");
		System.out.println("Not supposed to do anything! Just testing.");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("POST request to ReimbursementRequest Servlet");
		
		ObjectMapper om = new ObjectMapper();
		ReimbursementRequest Rr = om.readValue(request.getReader().readLine(), ReimbursementRequest.class);
		String type = String.valueOf(Rr.getType());
		String status = String.valueOf(Rr.getStatus());
		reimbursementService.createReimbursementRequest(Rr.getUserId(), Rr.getFirstName(), Rr.getLastName(), Rr.getAmount(), type, status);
	}
}
