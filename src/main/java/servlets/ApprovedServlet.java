package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import services.ReimbursementService;

import models.ReimbursementRequest;
import models.User;

public class ApprovedServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private ReimbursementService reimbursementService = new ReimbursementService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("GET request to Approved Servlet for userId");
		
		List<ReimbursementRequest> Rr = new ArrayList<>();
		String eIdString = request.getParameter("id");
		int eId = Integer.parseInt(eIdString);
		Rr = reimbursementService.viewApprovedReimbursements(eId);
		System.out.println(Rr);
		
		ObjectMapper om = new ObjectMapper();
		String RrJson = om.writeValueAsString(Rr);
		PrintWriter pw = response.getWriter();
		pw.write(RrJson);
		pw.close();
	}
}
