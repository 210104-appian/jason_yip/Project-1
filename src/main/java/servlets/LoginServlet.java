package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import services.ReimbursementService;
import services.UserService;
import models.ReimbursementRequest;
import models.User;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private UserService userService = new UserService();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("POST request to Login Servlet");
		
		ObjectMapper om = new ObjectMapper();
		User credentials = om.readValue(request.getReader(), User.class);
		User user = userService.findUserByUsernameAndPassword(credentials.getUsername(), credentials.getPassword());
		System.out.println("We've reached the login servlet");
		System.out.println("Our user: " + user);
		if(user!=null) {
			//send back token
			String token = om.writeValueAsString(user);
			try(PrintWriter pw = response.getWriter()){
				pw.write(token);
			}
		} else {
			response.sendError(401);
		}
	}
}
