# project-1-ers

## Project description:
This project is a full-stack web application that allows users to log in and handle reimbursement requests. 
It utilizes HTML, CSS, JavaScript, and Bootstrap for the front end, and uses Java, Java Servlets, JDBC, and SQL for the back end.
Dependency control is handled by Apache Maven, and Git is used for version control.

## Technologies:
- HTML
- CSS
- JavaScript
- Bootstrap
- AJAX
- Java
- SQL
- JDBC
- Servlets
- Apache Tomcat
- Apache Maven


## Features:
1. Employee
	- An Employee can login
	- An Employee can view the Employee Homepage
	- An Employee can logout
	- An Employee can submit a reimbursement request
	- An employee can view all reimbursement requests

2. Manager
	- A Manager can login
	- A manager can view the Manager Homepage
	- A manager can logout
	- A manager can view all pending requests from all employees

